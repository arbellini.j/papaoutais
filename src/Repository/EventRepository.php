<?php

namespace App\Repository;

use App\Entity\Event;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use \PDO;

/**
 * @method Event|null find($id, $lockMode = null, $lockVersion = null)
 * @method Event|null findOneBy(array $criteria, array $orderBy = null)
 * @method Event[]    findAll()
 * @method Event[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventRepository extends ServiceEntityRepository
{
    private $pdo;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Event::class);
        $this->pdo = new \PDO('pgsql:dbname=db;host=db;port=5432', 'postgres', 'password');
    }

    public function searchEvent($keyWord){

        if($keyWord != null){

            $word = explode(" ", $keyWord);

            $word1 = $word[0];

            $word2 = " ";

            $word3 = " ";

            $regex = "%";
            $regex2 = "";
            $regex3 = "";

            $arrayLength = sizeof($word);

            if ($arrayLength == 2){
                $word2 = $word[1];
                $regex2 = "%";
            }
            if($arrayLength == 3){
                $word2 = $word[1];
                $word3 = $word[2];
                $regex2 = "%";
                $regex3 = "%";
            }
           
            $query = $this->pdo->prepare("SELECT name, description
                                          FROM event
                                          WHERE  name ILIKE '".$regex.$word1.$regex."'
                                          OR name ILIKE '".$regex2.$word2.$regex2."'
                                          OR name ILIKE '".$regex3.$word3.$regex3."'");
           
            $query->execute();
            $data = $query->fetchall();
            return $data;
            
        }
        
    }

    // /**
    //  * @return Event[] Returns an array of Event objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Event
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
