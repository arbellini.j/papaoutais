<?php

namespace App\Entity;

use App\Repository\BetRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=BetRepository::class)
 */
class Bet
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=45)
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isPublic;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $status;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2)
     */
    private $totalStake;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isSponsored;

    /**
     * @ORM\ManyToOne(targetEntity=Event::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $event_idEvent;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $user_idUser_creator;

    /**
     * @ORM\OneToOne(targetEntity=BetChoice::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $winningChoice;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIsPublic(): ?bool
    {
        return $this->isPublic;
    }

    public function setIsPublic(bool $isPublic): self
    {
        $this->isPublic = $isPublic;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getTotalStake(): ?string
    {
        return $this->totalStake;
    }

    public function setTotalStake(string $totalStake): self
    {
        $this->totalStake = $totalStake;

        return $this;
    }

    public function getIsSponsored(): ?bool
    {
        return $this->isSponsored;
    }

    public function setIsSponsored(bool $isSponsored): self
    {
        $this->isSponsored = $isSponsored;

        return $this;
    }

    public function getEventIdEvent(): ?Event
    {
        return $this->event_idEvent;
    }

    public function setEventIdEvent(?Event $event_idEvent): self
    {
        $this->event_idEvent = $event_idEvent;

        return $this;
    }

    public function getUserIdUserCreator(): ?User
    {
        return $this->user_idUser_creator;
    }

    public function setUserIdUserCreator(?User $user_idUser_creator): self
    {
        $this->user_idUser_creator = $user_idUser_creator;

        return $this;
    }

    public function getWinningChoice(): ?BetChoice
    {
        return $this->winningChoice;
    }

    public function setWinningChoice(BetChoice $winningChoice): self
    {
        $this->winningChoice = $winningChoice;

        return $this;
    }
}
