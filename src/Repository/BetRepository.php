<?php

namespace App\Repository;

use App\Entity\Bet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use \PDO;

/**
 * @method Bet|null find($id, $lockMode = null, $lockVersion = null)
 * @method Bet|null findOneBy(array $criteria, array $orderBy = null)
 * @method Bet[]    findAll()
 * @method Bet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BetRepository extends ServiceEntityRepository
{
    private $pdo;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Bet::class);
        $this->pdo = new \PDO('pgsql:dbname=db;host=db;port=5432', 'postgres', 'password');
    }

    public function searchBet($keyWord){

        if($keyWord != null){

            $word = explode(" ", $keyWord);

            $word1 = $word[0];

            $word2 = " ";

            $word3 = " ";

            $regex = "%";
            $regex2 = "";
            $regex3 = "";

            $arrayLength = sizeof($word);

            if ($arrayLength == 2){
                $word2 = $word[1];
                $regex2 = "%";
            }
            if($arrayLength == 3){
                $word2 = $word[1];
                $word3 = $word[2];
                $regex2 = "%";
                $regex3 = "%";
            }
            
            $query = $this->pdo->prepare("SELECT name
                                          FROM bet
                                          WHERE name ILIKE '".$regex.$word1.$regex."'
                                          OR name ILIKE '".$regex2.$word2.$regex2."'
                                          OR name ILIKE '".$regex3.$word3.$regex3."'");
           
            $query->execute();
            $data = $query->fetchall();
            return $data;
        }

        
    }

    // /**
    //  * @return Bet[] Returns an array of Bet objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Bet
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
