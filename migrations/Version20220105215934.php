<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220105215934 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE "bet_choice_has_user" DROP CONSTRAINT bet_choice_has_user_pkey');
        $this->addSql('ALTER TABLE "bet_choice_has_user" ADD CONSTRAINT bet_choice_has_user_pkey PRIMARY KEY (user_id_user_id, id_bet_choice_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
