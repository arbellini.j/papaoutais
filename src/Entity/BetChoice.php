<?php

namespace App\Entity;

use App\Repository\BetChoiceRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BetChoiceRepository::class)
 */
class BetChoice
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=Bet::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $bet_idBet;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getBetIdBet(): ?Bet
    {
        return $this->bet_idBet;
    }

    public function setBetIdBet(?Bet $bet_idBet): self
    {
        $this->bet_idBet = $bet_idBet;

        return $this;
    }
}
