<?php

namespace App\Repository;

use App\Entity\BetInvitationStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BetInvitationStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method BetInvitationStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method BetInvitationStatus[]    findAll()
 * @method BetInvitationStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BetInvitationStatusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BetInvitationStatus::class);
    }

    // /**
    //  * @return BetInvitationStatus[] Returns an array of BetInvitationStatus objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BetInvitationStatus
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
