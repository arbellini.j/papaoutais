<?php

namespace App\Controller\Front;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Form\SearchBarType;
use App\Repository\CategoryRepository;
use App\Repository\EventRepository;
use App\Repository\BetRepository;

class DefaultController extends AbstractController
{
    #[Route('/', name: 'default')]
    public function index(CategoryRepository $categoryRepository, EventRepository $eventRepository, BetRepository $betRepository, Request $request): Response
    {
        $searchResultCat = null;
        $searchResultEvent = null;
        $searchResultBet = null;
        
        $form = $this->createForm(SearchBarType::class);
        $search = $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $searchResultCat = $categoryRepository->search($search->get('search')->getData());
            $searchResultEvent = $eventRepository->searchEvent($search->get('search')->getData());
            $searchResultBet = $betRepository->searchBet($search->get('search')->getData());
           
        }

        
        

        return $this->render('front/default/index.html.twig', [
            'searchResultCat' => $searchResultCat,
            'searchResultEvent' => $searchResultEvent,
            'searchResultBet' => $searchResultBet,
            'form' => $form->createView()
        ]);
         
    }
}
