<?php

namespace App\Repository;

use App\Entity\BetChoiceHasUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BetChoiceHasUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method BetChoiceHasUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method BetChoiceHasUser[]    findAll()
 * @method BetChoiceHasUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BetChoiceHasUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BetChoiceHasUser::class);
    }

    // /**
    //  * @return BetChoiceHasUser[] Returns an array of BetChoiceHasUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BetChoiceHasUser
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
