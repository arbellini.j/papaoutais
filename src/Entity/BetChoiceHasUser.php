<?php

namespace App\Entity;

use App\Repository\BetChoiceHasUserRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BetChoiceHasUserRepository::class)
 */
class BetChoiceHasUser
{
    /**
     * @ORM\Column(type="decimal", precision=5, scale=2)
     */
    private $stake;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2)
     */
    private $ratingAtThisMoment;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $user_idUser;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity=BetChoice::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_betChoice;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStake(): ?string
    {
        return $this->stake;
    }

    public function setStake(string $stake): self
    {
        $this->stake = $stake;

        return $this;
    }

    public function getRatingAtThisMoment(): ?string
    {
        return $this->ratingAtThisMoment;
    }

    public function setRatingAtThisMoment(string $ratingAtThisMoment): self
    {
        $this->ratingAtThisMoment = $ratingAtThisMoment;

        return $this;
    }

    public function getUserIdUser(): ?User
    {
        return $this->user_idUser;
    }

    public function setUserIdUser(?User $user_idUser): self
    {
        $this->user_idUser = $user_idUser;

        return $this;
    }

    public function getIdBetChoice(): ?BetChoice
    {
        return $this->id_betChoice;
    }

    public function setIdBetChoice(?BetChoice $id_betChoice): self
    {
        $this->id_betChoice = $id_betChoice;

        return $this;
    }
}
