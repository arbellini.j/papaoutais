<?php

namespace App\Repository;

use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Cache\Adapter\PdoAdapter;
use \PDO;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    private $pdo;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Category::class);
        $this->pdo = new \PDO('pgsql:dbname=db;host=db;port=5432', 'postgres', 'password');
    }

    /*
     * Recherche les catégories
     * @return void
    */
    public function search($keyWord){

        if($keyWord != null){

            $word = explode(" ", $keyWord);

            $word1 = $word[0];

            $word2 = "";

            $word3 = "";

            $regex = "%";
            $regex2 = "";
            $regex3 = "";

            $arrayLength = sizeof($word);



            if ($arrayLength == 2){
                $word2 = $word[1];
                $regex2 = "%";
            }
            if($arrayLength == 3){
                $word2 = $word[1];
                $word3 = $word[2];
                $regex2 = "%";
                $regex3 = "%";
            }
    
            $query = $this->pdo->prepare("SELECT name
                                          FROM category
                                          WHERE name ILIKE '".$regex.$word1.$regex."'
                                          OR name ILIKE '".$regex2.$word2.$regex2."'
                                          OR name ILIKE '".$regex3.$word3.$regex3."'");
            
            $query->execute();
            $data = $query->fetchall();
            return $data;
        }
           
    }

    // /**
    //  * @return Category[] Returns an array of Category objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Category
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
